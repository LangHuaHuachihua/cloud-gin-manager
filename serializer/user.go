package serializer

import "gitee.com/LangHuaHuachihua/cloud-gin-manager/models"

// User 用户序列化器
type User struct {
	ID        uint   `json:"id"`
	UserName  string `json:"user_name"`
	Nickname  string `json:"nickname"`
	Status    string `json:"status"`
	Avatar    string `json:"avatar"`
	CreatedAt int64  `json:"created_at"`
	Token     string `json:"token"`
}

// BuildUser 序列化用户
func BuildLoginUser(user models.User, token string) User {
	return User{
		ID:        user.ID,
		UserName:  user.UserName,
		Nickname:  user.Nickname,
		Status:    user.Status,
		Avatar:    user.Avatar,
		CreatedAt: user.CreatedAt.Unix(),
		Token:     token,
	}
}
func BuildRegisterUser(user models.User) User {
	return User{
		ID:        user.ID,
		UserName:  user.UserName,
		Nickname:  user.Nickname,
		Status:    user.Status,
		Avatar:    user.Avatar,
		CreatedAt: user.CreatedAt.Unix(),
	}
}

// BuildUserResponse 序列化用户响应
func BuildRegisterUserResponse(user models.User, code int) Response {
	return Response{
		Data: BuildRegisterUser(user),
		Msg:  "ok",
		Code: code,
	}
}
func BuildLoginUserResponse(user models.User, token string, code int) Response {
	return Response{
		Data: BuildLoginUser(user, token),
		Msg:  "ok",
		Code: code,
	}
}
