package utils_test

import (
	"os"
	"testing"

	"gitee.com/LangHuaHuachihua/cloud-gin-manager/utils"
	"github.com/stretchr/testify/assert"
)

func TestToken(t *testing.T) {
	shuold := assert.New(t)
	token, err := utils.SetToken("test", 1)
	if shuold.NoError(err) {
		t.Log(token)
	}
	claims, err := utils.CheckToken(token)
	if shuold.NoError(err) {
		t.Log(claims)
	}
}

func TestImge(t *testing.T) {
	filePath, err := os.Getwd()
	if err != nil {
		t.Log(err)
	}
	content, err := os.ReadFile(filePath + "/../conf/start_log.txt")
	if err != nil {
		t.Log("Error reading file:", err)
		return
	}
	t.Log(string(content))
	t.Log(filePath, err)
}
