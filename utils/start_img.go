package utils

import (
	"fmt"
	"os"
)

func Start_log() {
	log := Log()
	filePath, err := os.Getwd()
	if err != nil {
		log.Info("获取当前路径失败")
	}
	//项目启动时当前目录为项目根
	content, err := os.ReadFile(filePath + "/conf/start_log.txt")
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}
	log.Println(string(content))
}
