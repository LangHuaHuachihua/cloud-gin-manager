package utils

import (
	"fmt"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type MyClaims struct {
	Username string `json:"user_name"`
	Id       uint   `json:"id"`
	//Password string `json:"password"`
	jwt.RegisteredClaims
}

// 生成token
func SetToken(username string, Id uint) (string, error) {
	SetClaims := MyClaims{
		Username: username,
		//Password: password,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)), //有效时间
			IssuedAt:  jwt.NewNumericDate(time.Now()),                     //签发时间
			NotBefore: jwt.NewNumericDate(time.Now()),                     //生效时间
			Issuer:    os.Getenv("JWT_ISSUER"),                            //签发人
			Subject:   "somebody",                                         //主题
			ID:        string(rune(Id)),                                   //JWT ID用于标识该JWT
			Audience:  []string{"somebody_else"},                          //用户
		},
	}

	//使用指定的加密方式和声明类型创建新令牌
	tokenStruct := jwt.NewWithClaims(jwt.SigningMethodHS256, SetClaims)
	//获得完整的、签名的令牌
	token, err := tokenStruct.SignedString([]byte(os.Getenv("JWT_KEY")))
	if err != nil {
		logger.Error("生成token失败", err)
		return "", fmt.Errorf("生成token失败")
	}
	return token, nil

}

// 验证token
func CheckToken(token string) (*MyClaims, error) {
	//解析、验证并返回token。
	tokenObj, err := jwt.ParseWithClaims(token, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_KEY")), nil
	})

	if err != nil {
		logger.Warning("验证token失败", err)
		return nil, fmt.Errorf("验证token失败")
	}

	if claims, ok := tokenObj.Claims.(*MyClaims); ok && tokenObj.Valid {
		fmt.Printf("%v  %v  %v\n", claims.Username, claims.ID, claims.RegisteredClaims)
		return claims, nil
	} else {
		return nil, fmt.Errorf("解析token失败")
	}
}
