package main

import "gitee.com/LangHuaHuachihua/cloud-gin-manager/cmd"

func main() {

	if err := cmd.ServiceCmd.Execute(); err != nil {
		return
	}
}
