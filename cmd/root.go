package cmd

import (
	"fmt"
	

	"github.com/spf13/cobra"
	"gitee.com/LangHuaHuachihua/cloud-gin-manager/version"
)

var vers bool

var RootCmd = &cobra.Command{
	Use:   "tianqing",
	Short: "天擎综合运维平台 Api",
	Long:  "天擎综合运维平台 Api",
	RunE: func(cmd *cobra.Command, args []string) error {
		if vers {
			fmt.Println(version.FullVersion())
			return nil
		}
		return nil
	},
}

func init() {
	RootCmd.PersistentFlags().BoolVarP(&vers, "version", "v", false, "print demo-api version")
}
